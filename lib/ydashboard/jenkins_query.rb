
require "jenkins_api_client"
require "yaml"

module Ydashboard
  class JenkinsQuery
    PUBLIC_JENKINS = "https://ci.opensuse.org"
    INTERNAL_JENKINS = "https://ci.suse.de"

    attr_reader :job_pattern, :internal_jenkins

    def initialize(job_pattern: //, internal_jenkins: false)
      @job_pattern = job_pattern
      @internal_jenkins = internal_jenkins
    end

    def run
      url = internal_jenkins ? INTERNAL_JENKINS : PUBLIC_JENKINS
      jenkins = JenkinsApi::Client.new(server_url: url)
      jobs = jenkins.job.list_all_with_details

      jobs.select! { |j| j["name"] =~ job_pattern }

      Rails.logger.info "Found #{jobs.size} Jenkins jobs"

      # download console logs for failed jobs
      jobs.each do |j|
        next if j["color"] != "red"
        Rails.logger.info "Downloading log for failed job #{j["name"]}"
        j["build"] = jenkins.job.get_current_build_number(j["name"])
        j["console"] = jenkins.job.get_console_output(j["name"], j["build"])
        # replace the DOS new lines otherwise the regexp $ won't match
        j["console"]["output"].gsub!(/\r\n/, "\n")
        Rails.logger.info "Downloaded log size: #{j["console"]["output"].size}"
      end

      save_result(jobs, internal_jenkins) if ENV["YDASHBOARD_SAVE_JENKINS_RESPONSE"]

      jobs
    end

    def save_result(response, internal)
      fn = File.join(Rails.root, "tmp", Time.now().strftime("Jenkins-#{internal ? "int" : "ext"}-%F-%H-%M-%S.yml"))
      Rails.logger.info "Saving to #{fn}"
      File.write(fn, response.to_yaml)
    end
  end
end







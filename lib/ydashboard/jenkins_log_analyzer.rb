
require "yaml"

module Ydashboard
  class JenkinsLogAnalyzer
    attr_reader :log

    def initialize(log)
      @log = log
    end

    def analyze
      errors = []
      actions = []

      rules.each do |rule|
        next unless log =~ Regexp.new(rule["match"])

        errors << rule["desc"]
        actions << rule["action"]
      end

      errors << "Unknown error" if errors.empty?

      [errors, actions]
    end

    # Note: use https://github.com/<author>.png?size=32 image in UI
    def author
      if log =~ /Started by GitHub push by (.*)$/
        Regexp.last_match[1]
      else
        nil
      end
    end

  private

    attr_reader :rules

    def rules
      @rules ||= YAML.load_file(File.join(Rails.root, "config", "jenkins_rules.yml"))
    end
  end
end

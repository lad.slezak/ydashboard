# A task for exporting the Bugzilla statistics to db/seeds.rb file,
# run "bundle exec rake db:seed" to import the file
# (do not export everything, the other data are imported from external
# sources anyway and they might potentially include some internal data)

namespace :export do
  desc 'Export Bugzilla stats do db/seeds.rb (use rake db:seed to import)'
  task seeds: :environment do
    SeedDump.dump(BugStat, file: 'db/seeds.rb')
    SeedDump.dump(ClosedBugStat, file: 'db/seeds.rb', append: true)
  end
end

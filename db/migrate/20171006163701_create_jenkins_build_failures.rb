class CreateJenkinsBuildFailures < ActiveRecord::Migration[5.0]
  def change
    create_table :jenkins_build_failures do |t|
      t.integer :build, null: false
      t.text :failures
      t.text :actions
      t.text :author

      t.timestamps
    end
  end
end

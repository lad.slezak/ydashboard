class CreateBugs < ActiveRecord::Migration[5.0]
  def change
    create_table :bugs do |t|
      t.integer :bug_id
      t.integer :priority
      t.string :product
      t.string :assignee
      t.string :status
      t.string :summary
      t.datetime :change_date

      t.timestamps
    end
  end
end

#! /bin/bash

# make sure we are always at the app root
cd "${BASH_SOURCE%/*}/.."

COMPOSE_FILES="-f docker-compose.yml -f docker-compose.production.yml"

# build the Docker images
docker-compose $COMPOSE_FILES build

# create the DB
docker-compose $COMPOSE_FILES run app bundle exec rake db:create
# load the DB schema
docker-compose $COMPOSE_FILES run app bundle exec rake db:schema:load
# load the Bugzilla history stats
docker-compose $COMPOSE_FILES run app bundle exec rake db:seed


# do not restart failed jobs, they are started repeatedly anyway
Delayed::Worker.max_attempts = 1

# less frequent checks, the jobs run just several times per hour
# the default is 5 seconds
Delayed::Worker.sleep_delay = 10

# max. run time for a job, the default is 4 hours which could block processing
Delayed::Worker.max_run_time = 5.minutes

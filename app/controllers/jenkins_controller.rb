class JenkinsController < ApplicationController
  def index
    # FIXME: DRY
    if JenkinsStatus.where(internal: false).exists?
      public_failures = JenkinsBuildFailure.joins(:jenkins_status)
        .where(jenkins_statuses: {internal: false})

      @public_yast_failures = []
      @public_libyui_failures = []
      @public_other_failures = []

      public_failures.each do |f|
        name = f.jenkins_status.name
        if name.start_with?("yast")
          @public_yast_failures << f
        elsif name.start_with?("libyui")
          @public_libyui_failures << f
        else
          @public_other_failures << f
        end
      end

      Rails.logger.debug { "@public_yast_failures: #{@public_yast_failures}" }
      Rails.logger.debug { "@public_libyui_failures: #{@public_libyui_failures}" }
      Rails.logger.debug { "@public_other_failures: #{@public_other_failures}" }
    end

    if JenkinsStatus.where(internal: true).exists?
      internal_failures = JenkinsBuildFailure.joins(:jenkins_status)
        .where(jenkins_statuses: {internal: true})

      @internal_yast_failures = []
      @internal_libyui_failures = []
      @internal_other_failures = []

      internal_failures.each do |f|
        name = f.jenkins_status.name
        if name.start_with?("yast")
          @internal_yast_failures << f
        elsif name.start_with?("libyui")
          @internal_libyui_failures << f
        else
          @internal_other_failures << f
        end
      end

      Rails.logger.debug { "@internal_yast_failures: #{@internal_yast_failures}" }
      Rails.logger.debug { "@internal_libyui_failures: #{@internal_libyui_failures}" }
      Rails.logger.debug { "@internal_other_failures: #{@internal_other_failures}" }
    end
  end
end

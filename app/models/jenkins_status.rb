class JenkinsStatus < ApplicationRecord
  has_many :jenkins_build_failures, dependent: :destroy

  PUBLIC_JENKINS = "https://ci.opensuse.org".freeze
  INTERNAL_JENKINS = "https://ci.suse.de".freeze

  def url
    self.internal ? INTERNAL_JENKINS : PUBLIC_JENKINS
  end
end

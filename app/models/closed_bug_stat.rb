class ClosedBugStat < ApplicationRecord
  def reset
    self.fixed = 0
    self.resolved_invalid = 0
    self.wontfix = 0
    self.noresponse = 0
    self.upstream = 0
    self.feature = 0
    self.duplicate = 0
    self.worksforme = 0
    self.moved = 0
  end

  def total_closed
    fixed + resolved_invalid + wontfix + noresponse + upstream + feature \
      + duplicate + worksforme + moved
  end
end

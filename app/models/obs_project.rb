class ObsProject < ApplicationRecord
  has_many :obs_repo_statuses, dependent: :destroy
end

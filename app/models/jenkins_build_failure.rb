class JenkinsBuildFailure < ApplicationRecord
  belongs_to :jenkins_status

  def log_url
    uri = URI(jenkins_status.url)
    uri.path = "/job/#{jenkins_status.name}/#{build}/console"
    uri
  end

  def job_url
    uri = URI(jenkins_status.url)
    uri.path = "/job/#{jenkins_status.name}"
    uri
  end
end
